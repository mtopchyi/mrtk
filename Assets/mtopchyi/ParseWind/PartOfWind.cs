using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartOfWind : MonoBehaviour
{
    [SerializeField] private Transform startPosition;
    [SerializeField] private Transform endPosition;

  
    
    public void ChangePosition(float value)
    {
        var start = startPosition.position;
        var end = endPosition.position;

        transform.position = Vector3.Lerp(start, end, value);
    }
}
