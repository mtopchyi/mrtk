using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.MixedReality.Toolkit.Experimental.UI;
using Microsoft.MixedReality.Toolkit.UI;
using UnityEngine;

public class ParseWind : MonoBehaviour
{
    [SerializeField] private StepSlider slider;

    [SerializeField] private List<PartOfWind> parts;


    public void OnChangeSlider()
    {
        foreach (var part in parts)
        {
            part.ChangePosition(slider.SliderValue);
        }
    }
}
