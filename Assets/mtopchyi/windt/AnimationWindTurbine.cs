using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationWindTurbine : MonoBehaviour
{
    [SerializeField] private GameObject blades;
    
    [SerializeField] private bool isAnimated;

    [SerializeField] private float speed = 9f;


    private float xAngles = 0f;
    
    public void ToggleAnimation()
    {
        isAnimated = !isAnimated;
    }

    private void Animate()
    {
        var eulers = blades.transform.eulerAngles;
        xAngles += speed * Time.deltaTime;
        blades.transform.Rotate(0, speed * Time.deltaTime, 0, Space.Self);
    }
    
    private void Awake()
    {
        isAnimated = false;
    }
    
    

    private void Update()
    {
        if(isAnimated)
            Animate();
    }
}
