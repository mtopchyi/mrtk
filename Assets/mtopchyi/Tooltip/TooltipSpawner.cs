using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TooltipSpawner : Microsoft.MixedReality.Toolkit.UI.ToolTipSpawner
{
    public event Action<TooltipSpawner> Tapped;
    public bool IsActive = false;
    public bool CanActive = true;

    [SerializeField] private Transform anchorTransform;
    [SerializeField] private Transform parentTransform;

    private float sizeScale;
    
    public void ChangeState()
    {
        if(!IsActive)
            base.HandleTap();
        IsActive = false;
    }
    
    public void Tap()
    {
        if (!CanActive)
            return;
        
        base.HandleTap();
        IsActive = !IsActive;

    }
    
    protected override void HandleTap()
    {
        if (!CanActive)
            return;
        
        base.HandleTap();
        IsActive = !IsActive;

        Tapped?.Invoke(this);
    }

    private void Awake()
    {
        Tap();
        Tap();
        sizeScale = transform.localScale.x;
    }

    private void Update()
    {
        transform.position = anchorTransform.position;
        
        transform.localScale = sizeScale * parentTransform.localScale;
    }
}
