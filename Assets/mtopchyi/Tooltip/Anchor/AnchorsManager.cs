using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.UI;
using UnityEngine;

public class AnchorsManager : MonoBehaviour
{
    public static AnchorsManager Instance;
    
    [SerializeField] private List<TooltipSpawner> anchors;

    [SerializeField] private TooltipSpawner activeAnchorTooltip;

    private bool isAll = false;
    
    public void ChangeStateAll()
    {
        isAll = !isAll;
        AllAnchorsTap();
    }
    
    
    
    private void Awake()
    {
        if (Instance == null)
            Instance = this;

        activeAnchorTooltip = null;
    }
    private void Start()
    {
        SubscribeOnEventAnchors();
    }

    private void AllAnchorsTap()
    {
        foreach (var anchor in anchors)
        {
            
            anchor.ChangeState();
            anchor.CanActive = !isAll;
        }

        activeAnchorTooltip = null;
    }
    private void SubscribeOnEventAnchors()
    {
        foreach (var anchor in anchors)
        {
            
            anchor.Tapped += ChangeActiveAnchor;
        }
    }
    private void ChangeActiveAnchor(TooltipSpawner anchorTooltip)
    {

        if (isAll)
            return;
        
        
        if (activeAnchorTooltip != null)
        {
            if(activeAnchorTooltip.IsActive)
                activeAnchorTooltip.Tap();
            
            activeAnchorTooltip = activeAnchorTooltip == anchorTooltip ? null : anchorTooltip;
        }
        else
            activeAnchorTooltip = anchorTooltip;        
    }
}
